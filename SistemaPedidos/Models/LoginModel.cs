﻿using SistemaPedidos.Uteis;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SistemaPedidos.Models
{
    public class LoginModel
    {
        public string idLogin { get; set; }
        
        [Required(ErrorMessage="Informe o usuário!")]
        [DataType(DataType.Text)]
        public string Usuario { get; set; }

        [Required(ErrorMessage = "Informe a senha!")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        public bool validaLogin()
        {
            string sql = $"SELECT idLogin FROM Login (NOLOCK) WHERE usuarioLogin = '{Usuario}' AND senhaLogin = '{Senha}'";
            DAL objDAL = new DAL();
            DataTable dt = objDAL.RetDataTable(sql);

            var retorno = dt.Rows.Count > 0;

            if(retorno)
                idLogin = dt.Rows[0]["idLogin"].ToString();

            return retorno;
        }
    }
}
