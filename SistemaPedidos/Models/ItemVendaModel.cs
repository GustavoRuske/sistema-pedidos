﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaPedidos.Models
{
    public class ItemPedidoModel
    {
        public string idProduto { get; set; }
        public string QtdProduto { get; set; }
        public string DescProduto { get; set; }

        public string PrecoUnitario { get; set; }
        public string Total { get; set; }
        public string idPedido { get; set; }

        public string ItensToJson(string idPedido)
        {
            var json = "";
            return json;
        }
    }   
}
