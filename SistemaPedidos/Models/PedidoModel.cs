﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaPedidos.Uteis;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaPedidos.Models
{
    public class PedidoModel
    {
        [Required(ErrorMessage = "Informe o cliente")]
        public string IdCliente { get; set; }
        public string TotalPedido { get; set; }
        public string IdPedido { get; set; }
        public string JsonItens { get; set; }

        public List<ProdutoModel> RetornarListaProdutos()
        {
            return new ProdutoModel().ListarTodosProdutos();
        }

        internal List<PedidoModel> ListagemPedidos()
        {
            List<PedidoModel> lista = new List<PedidoModel>();
            PedidoModel item;
            DAL objDAL = new DAL();

            string sql = "SELECT idPedido, c.nomeCliente as cliente, totalPedido FROM Pedido p (NOLOCK) INNER JOIN Cliente c (NOLOCK) on p.idCliente = c.idCliente";
            DataTable dt = objDAL.RetDataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                item = new PedidoModel
                {
                    IdPedido = dt.Rows[i]["idPedido"].ToString(),
                    IdCliente = dt.Rows[i]["cliente"].ToString(),
                    TotalPedido = dt.Rows[i]["totalPedido"].ToString()
                };
                lista.Add(item);
            }

            return lista;
        }

        public List<ClienteModel> RetornarListaClientes()
        {
            return new ClienteModel().ListarTodosClientes();
        }
        
        public void GravarPedido()
        {
            DAL objDAL = new DAL();

            if (IdPedido == null)
            {
                InserirPedido(objDAL);
            }
            else
            {
                AlterarPedido(objDAL);
            }
            
        }

        private void AlterarPedido(DAL objDAL)
        {
            throw new NotImplementedException();
        }

        public void InserirPedido(DAL objDAL)
        {

            var sql = $"INSERT INTO Pedido (totalPedido,idCliente) VALUES ({TotalPedido}, {IdCliente})";
            objDAL.ExecutarComandoSQL(sql);

            sql = $"SELECT TOP 1 idPedido FROM Pedido (NOLOCK) WHERE idCliente = {IdCliente} ORDER BY idPedido DESC";
            DataTable dt = objDAL.RetDataTable(sql);
            var id = dt.Rows[0]["idPedido"].ToString();

            List<ItemPedidoModel> ItensPedido = JsonConvert.DeserializeObject<List<ItemPedidoModel>>(JsonItens);

            //var jsonItens = JsonConvert.SerializeObject(ItensPedido);

            InserirItem(objDAL, ItensPedido, id);

        }

        public void DeletaItens(DAL objDAL)
        {
            var sql = $"DELETE FROM ItemPedido WHERE idPedido = '{IdPedido}'";
            objDAL.ExecutarComandoSQL(sql);
        }

        public void InserirItem(DAL objDAL, List<ItemPedidoModel> itensPedido, string idPedido)
        {
            var sql = "";
            for (int i = 0; i < itensPedido.Count; i++)
            {
                sql = "INSERT INTO ItemPedido (idPedido ,idProduto ,qtdProduto ,precoUnitItem, totalItem)" +
                    "VALUES" +
                    $"({idPedido}" +
                    $",{itensPedido[i].idProduto.ToString()}" +
                    $",{itensPedido[i].QtdProduto.ToString()}" +
                    $",{itensPedido[i].PrecoUnitario.ToString()}" +
                    $",{itensPedido[i].Total.ToString()})";

                objDAL.ExecutarComandoSQL(sql);
            }
        }

        public PedidoModel CarregaPedido(string idPedido)
        {
            PedidoModel item;
            DAL objDAL = new DAL();

            string sql = $"SELECT idPedido, idCliente, totalPedido FROM Pedido (NOLOCK) WHERE idPedido = {idPedido}";
            DataTable dt = objDAL.RetDataTable(sql);

            item = new PedidoModel
            {
                IdPedido = dt.Rows[0]["idPedido"].ToString(),
                IdCliente = dt.Rows[0]["idCliente"].ToString(),
                TotalPedido = dt.Rows[0]["totalPedido"].ToString()
            };

            ItemPedidoModel itens = new ItemPedidoModel();

            JsonItens = itens.ItensToJson(idPedido);

            return item;
        }

    }
}
