﻿using SistemaPedidos.Uteis;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SistemaPedidos.Models
{
    public class ProdutoModel
    {
        public string IdProduto { get; set; }

        public string DescProduto { get; set; }

        public decimal PrecoUnitarioProduto { get; set; }

        public int MultiploProduto { get; set; }

        public List<ProdutoModel> ListarTodosProdutos()
        {
            List<ProdutoModel> lista = new List<ProdutoModel>();
            ProdutoModel item;
            DAL objDAL = new DAL();

            string sql = "SELECT idProduto, descProduto, precoUnitProduto, ISNULL(multiploProduto, 0) as multiploProduto FROM Produto (NOLOCK) ORDER BY idProduto";
            DataTable dt = objDAL.RetDataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                item = new ProdutoModel
                {
                    IdProduto = dt.Rows[i]["idProduto"].ToString(),
                    DescProduto = dt.Rows[i]["descProduto"].ToString(),
                    PrecoUnitarioProduto = decimal.Parse(dt.Rows[i]["precoUnitProduto"].ToString()),
                    MultiploProduto = int.Parse( dt.Rows[i]["multiploProduto"].ToString())
                };
                lista.Add(item);
            }

            return lista;
        }
        public List<ProdutoModel> ListarProdutosFiltro(string descricao)
        {
            List<ProdutoModel> lista = new List<ProdutoModel>();
            ProdutoModel item;
            DAL objDAL = new DAL();

            if(descricao != null)
            {
                string sql = $"SELECT idProduto, descProduto, precoUnitProduto, ISNULL(multiploProduto, 0) as multiploProduto FROM Produto (NOLOCK) WHERE descProduto LIKE '%{descricao.Trim()}%' ORDER BY descProduto";
                DataTable dt = objDAL.RetDataTable(sql);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    item = new ProdutoModel
                    {
                        IdProduto = dt.Rows[i]["idProduto"].ToString(),
                        DescProduto = dt.Rows[i]["descProduto"].ToString(),
                        PrecoUnitarioProduto = decimal.Parse(dt.Rows[i]["precoUnitProduto"].ToString()),
                        MultiploProduto = int.Parse(dt.Rows[i]["multiploProduto"].ToString())
                    };
                    lista.Add(item);
                }

            }

            return lista;
        }

    }
}
