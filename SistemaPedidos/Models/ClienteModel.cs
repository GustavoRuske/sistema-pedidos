﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using SistemaPedidos.Uteis;

namespace SistemaPedidos.Models
{
    public class ClienteModel
    {
        public string idCliente { get; set; }
        public string nomeCliente { get; set; }

        public List<ClienteModel> ListarTodosClientes()
        {
            List<ClienteModel> lista = new List<ClienteModel>();
            ClienteModel item;
            DAL objDAL = new DAL();
            
            string sql = "SELECT idCliente, nomeCliente FROM Cliente (NOLOCK)";
            DataTable dt = objDAL.RetDataTable(sql);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                item = new ClienteModel
                {
                    idCliente = dt.Rows[i]["idCliente"].ToString(),
                    nomeCliente = dt.Rows[i]["nomeCliente"].ToString()
                };
                lista.Add(item);
            }

            return lista;
        }
    }
}
