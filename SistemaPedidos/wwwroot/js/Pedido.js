﻿var classError = "has-error";
var valorPadraoPrd = "";
var qtdPadraoPrd = "";
var idProdSel = ""

$("#selProduto")
    .change(function () {
        var str = "";
        $("select option:selected").each(function () {
            str = $(this).val() + " ";
        });
        str = str.trim();
        if (str != "") {

            idProdSel = str.split('|')[0].trim();
            valorPadraoPrd = str.split('|')[1].trim();
            qtdPadraoPrd = str.split('|')[2].trim();

            $("#txtValorProduto").val(valorPadraoPrd);

            if (qtdPadraoPrd != 0)
                $("#txtQuantidade").val(qtdPadraoPrd);
            else
                $("#txtQuantidade").val(null);
        }

    })
    .change();

$("#txtQuantidade").bind("keyup blur focus", function (e) {
    e.preventDefault();
    var expre = /[^\d]/g;
    $(this).val($(this).val().replace(expre, ''));
});

$("#txtValorProduto").blur(ValidaRentabilidade);
$("#txtQuantidade").blur(ValidaQuantidade);

function Voltar() {
    window.location.href = '../Pedido/Index';
}

function AdicionarProduto() {

    var verificaoProd = ValidaProduto();

    if (verificaoProd)
        return;

    if ($("#spnRentab").text() == "Rentabilidade Ruim") {
        $("#txtValorProduto").val(valorPadraoPrd);
        swal("Alerta!", "Não é permitido inserir um produto com a Rentabilidade ruim!", "error");
        return;
    }

    var idProduto = "";
    var txtProduto = "";

    var QtdeProduto = $("#txtQuantidade").val();
    var vlrProduto = $("#txtValorProduto").val();
    vlrProduto = vlrProduto.replace('.', '');
    vlrProduto = vlrProduto.replace(',', '.');
    var vlrTotal = parseFloat(QtdeProduto * vlrProduto).toFixed(2);

    $.each($("#selProduto option:selected"), function () {
        idProduto = $(this).val();
        txtProduto = $(this).text();
    });

    //Adiciona uma nova linha na table
    AdicionarLinhaTable(idProduto, txtProduto, QtdeProduto, vlrProduto, vlrTotal);

    //Atualiza os totais na tela
    var totalPedido = $("#totalPedido").text();
    totalPedido = totalPedido.replace('.', '');
    totalPedido = totalPedido.replace(',', '.');

    var total = parseFloat(totalPedido) + parseFloat(vlrTotal);

    $("#totalPedido").text(total);
    $("#txtTotal").val(total);

    LimparProduto();
}

function ItensToJson() {

    //Percorre a table e monta um Json com os dados
    var Itens = new Object();
    Itens.Produtos = new Array();

    $("table tbody tr").each(function () {
        var tdTBody = $(this).children('td');

        var idProduto = tdTBody[0].textContent.split("|")[0].trim();
        var QtdeProduto = tdTBody[2].textContent.trim();
        var PrecoUnitario = tdTBody[3].textContent.trim();
        var ValorTotal = tdTBody[4].textContent.trim();

        Itens.Produtos.push({
            "idProduto": idProduto,
            "QtdProduto": QtdeProduto,
            "PrecoUnitario": PrecoUnitario,
            "Total": ValorTotal
        });

    })

    $("#JsonItens").val(JSON.stringify(Itens.Produtos));

}

function RegistrarPedido() {

    if (ValidaCliente())
        return;

    ItensToJson();
}

function RemoverPedido(idPedido) {
    window.location.href = '../Pedido/Excluir/' + idPedido;
}

function AdicionarLinhaTable(idProduto, txtProduto, QtdeProduto, vlrProduto, vlrTotal) {
    var novaTR = $("<tr>");

    var col = "";
    col += '<td style="display:none;"> ' + idProduto + ' </td>';
    col += '<td> ' + txtProduto + ' </td>';
    col += '<td> ' + QtdeProduto + ' </td>';
    col += '<td> ' + vlrProduto + ' </td>';
    col += '<td> ' + vlrTotal + ' </td>';
    col += '<td><button class="btn btn-primary btn-danger" type="button" onclick="RemoverProduto(this)">Remover</button> </td>';
    novaTR.append(col);

    $("#tabelaProdutos tbody").append(novaTR);
}

function ValidaCliente() {
    var retorno = false;

    $('#selCliente').parent().removeClass(classError);
    var selCli = $('#selCliente').val();

    if (!selCli) {
        retorno = true;
        $('#selCliente').parent().addClass(classError);
    }

    return retorno;
}

function ValidaProduto() {

    var retorno = false;

    $('#selProduto').parent().removeClass(classError);
    $('#txtValorProduto').parent().removeClass(classError);
    $('#txtQuantidade').parent().removeClass(classError);

    var selPrd = $('#selProduto').val();
    var vlrPrd = $('#txtValorProduto').val();
    var qtdPrd = $('#txtQuantidade').val();


    if (!selPrd) {
        retorno = true;
        $('#selProduto').parent().addClass(classError);
    }

    if (!vlrPrd || vlrPrd == 0) {
        retorno = true;
        $('#txtValorProduto').parent().addClass(classError);
    }

    if (!qtdPrd || qtdPrd == 0) {
        retorno = true;
        $('#txtQuantidade').parent().addClass(classError);
    }

    $("table tbody tr").each(function () {

        var tdTBody = $(this).children('td');

        var idProduto = tdTBody[0].textContent.split("|")[0].trim();

        if (idProdSel == idProduto) {
            retorno = true;
            swal("Alerta!", "Este produto já está inserido no pedido!", "error");
        }
    })


    return retorno;
}

function LimparProduto() {
    $('#selProduto').val("");
    $('#txtValorProduto').val("");
    $('#txtQuantidade').val("");

    DefaultRentabilidade();
}

function RemoverProduto(tr_tb) {

    var totalPedido = $("#txtTotal").val();

    var tr = $(tr_tb).closest('tr');

    var tdTBody = $(tr).children('td');

    var totalItem = tdTBody[4].textContent.trim();

    var newTotalPedido = parseFloat(totalPedido) - parseFloat(totalItem)

    $("#txtTotal").val(newTotalPedido);
    $("#totalPedido").text(newTotalPedido);

    //tr.fadeOut(400, function () { tr.remove();  });
    tr.remove();

}

function ValidaRentabilidade() {

    DefaultRentabilidade();

    var produtoSel = $("#selProduto").val();
    var valorUnitDigitado = $("#txtValorProduto").val().replace(',', '.');
    var valorPadraoPrdTemp = valorPadraoPrd.replace(',', '.');

    if (valorUnitDigitado.indexOf(".") == -1 ) {
        valorUnitDigitado = valorUnitDigitado + ".00";
    }

    valorUnitDigitado = parseFloat(valorUnitDigitado);
    valorPadraoPrdTemp = parseFloat(valorPadraoPrdTemp);

    $("#spnRentab").attr('class', '');

    if (!produtoSel) {
        return;
    }

    if (valorUnitDigitado > valorPadraoPrdTemp) {
        $("#spnRentab").addClass("label label-success");
        $("#spnRentab").text("Rentabilidade Otima");
    } else {
        if (valorUnitDigitado >= valorPadraoPrdTemp * 0.9) {
            $("#spnRentab").addClass("label label-warning");
            $("#spnRentab").text("Rentabilidade Boa");
        } else {
            $("#spnRentab").addClass("label label-danger");
            $("#spnRentab").text("Rentabilidade Ruim");
        }

    }
}

function DefaultRentabilidade() {

    $("#spnRentab").attr('class', '');
    $("#spnRentab").addClass("label label-default");
    $("#spnRentab").text("Rentabilidade");

}

function ValidaQuantidade() {

    var produtoSel = $("#selProduto").val();

    if (!produtoSel) {
        return;
    }

    var qtdDigitada = $("#txtQuantidade").val();

    if (!qtdDigitada || qtdDigitada == 0) {
        swal("Alerta!", "Por favor, informe a quantidade!", "error");
    }

    if (qtdPadraoPrd != 0) {

        if (qtdDigitada % qtdPadraoPrd != 0) {
            $("#txtQuantidade").val("0");
            swal("Alerta!", "É permitido somente informar uma quantidade múltiplo de " + qtdPadraoPrd.trim(), "error");
        }
    }

}