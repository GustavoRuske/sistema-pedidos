﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace SistemaPedidos.Uteis
{
    //Data Access Layer
    public class DAL
    {
        private static string Server = "ruske.database.windows.net";
        private static string Database = "db-ruske";
        private static string ConnectionString = $"Data Source = {Server}; Initial Catalog = {Database}; Integrated Security = False; User ID = adm.ruske; Password=rusk3.GUSTAVO;" +
                                                  "Connect Timeout = 200; Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static SqlConnection Connection;

        

        public DAL()
        {
            Connection = new SqlConnection(ConnectionString);
            Connection.Open();
        }

        //Espera um parâmetro do tipo string 
        //contendo um comando SQL do tipo SELECT
        public DataTable RetDataTable(string sql)
        {
            DataTable data = new DataTable();
            SqlCommand Command = new SqlCommand(sql, Connection);
            SqlDataAdapter da = new SqlDataAdapter(Command);
            da.Fill(data);
            return data;
        }

        //Espera um parâmetro do tipo string 
        //contendo um comando SQL do tipo INSERT, UPDATE, DELETE
        public void ExecutarComandoSQL(string sql)
        {
            SqlCommand Command = new SqlCommand(sql, Connection);
            Command.ExecuteNonQuery();
        }
    }
}
