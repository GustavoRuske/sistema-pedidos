﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SistemaPedidos.Models;

namespace SistemaPedidos.Controllers
{
    public class PedidoController : Controller
    {
        [HttpGet]
        public IActionResult Registrar()
        {
            CarregarViewBag();
            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.ListaVendas = new PedidoModel().ListagemPedidos();
            return View();
        }

        [HttpPost]
        public IActionResult Registrar(PedidoModel pedido)
        {
            if (ModelState.IsValid)
            {
                pedido.GravarPedido();
                return RedirectToAction("Index");
                
            }
            CarregarViewBag();
            return View();
        }

        private void CarregarViewBag()
        {
            ViewBag.ListaClientes = new PedidoModel().RetornarListaClientes();
            ViewBag.ListaProdutos = new PedidoModel().RetornarListaProdutos();
        }
    }
}